# Lexcube: Interactive Visualization and Exploration of High-Resolution Remote Sensing and Socioeconomic Data Sets

## Introduction
The integration of high-resolution remote sensing and socioeconomic data sets presents significant challenges due to the increasing spatial and temporal resolution of data and the need for effective visualization tools. To address these challenges, we developed Lexcube, a client-server application designed to facilitate the interactive exploration and visualization of large data cubes. Lexcube aims to enhance the accessibility and usability of these data sets for researchers and other stakeholders.

## Objectives
The primary objectives of Lexcube include:

1. **Develop a Centralized Platform for Data Visualization**: Create Lexcube to integrate and visualize large multivariate data cubes, enabling users to gain insights from high-resolution remote sensing and socioeconomic data sets.

2. **Enable Interactive Data Exploration**: Provide tools for users to interactively explore data cubes, including features such as animations, customizable color maps, and data set selection, all within a user-friendly interface.

3. **Support Large-Scale Data Handling**: Design Lexcube to efficiently handle large datasets, such as the Earth System Data Cube (~300 GB), ensuring robust performance even with high user traffic and weak network connections.

4. **Promote Open Science and FAIR Principles**: Ensure Lexcube adheres to FAIR (Findable, Accessible, Interoperable, Reusable) principles by offering an open-access platform with plans for open-source release, thereby supporting the broader scientific community.

## Key Features

- **Lexcube Online Platform**: A user-friendly interface allowing users to visualize and explore large data cubes interactively. The platform is accessible via [lexcube.org](https://www.lexcube.org) and supports both desktop and mobile devices.

- **Advanced Visualization Tools**: Lexcube uses a tiling approach to pre-generate data, ensuring minimal latency during interactions. It also includes features for customizing visualizations, such as adjusting color maps and selecting display quality.

## Outcomes
Lexcube has been publicly available at [lexcube.org](https://www.lexcube.org) since May 2022, and it provides the following functionalities:

- **Interactive Data Exploration**: Users can explore data cubes interactively, utilizing tools to manipulate the 3D data cube and view different perspectives on the data.
- **Customizable Visualizations**: Users can adjust color maps, play animations through time, and customize the display settings to suit their needs.
- **Scalable Performance**: The platform efficiently handles large data sets and multiple users, ensuring a smooth experience even under heavy load.

## Challenges and Gaps
While the Lexcube pilot project successfully delivered a functional platform, challenges remain, particularly in expanding functionality. The open-source release has been postponed to later in 2023 to allow for code restructuring and user-friendly improvements. Additionally, the feature for visualizing multiple data cubes simultaneously is under development but has not yet been completed.

